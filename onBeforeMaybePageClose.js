/** @license Apache-2.0 */
// @ts-check
/**
 * Executes the callback when there is a chance that the page is about to get
 * closed, i.e. maybe this is the last chance to execute something,
 * in particular `webxdc.sendUpdate()`.
 * Yes, this means that `callback` might get executed several times, on
 * occasions where the app didn't actually get closed.
 * @param {() => void} callback
 * @returns {() => void} an "undo" function
 */
export function onBeforeMaybePageClose(callback) {
	/** @type {Array<() => void>} */
	const onUndo = [];

	onUndo.push(
		onVisibilitychangeHidden(window, callback)
	);

	// A workaround for https://github.com/deltachat/deltachat-desktop/issues/3321
	// `webxdc.sendUpdate()` doesn't work inside of `visibilitychange` listener
	// when the listener is added inside of an iframe, so let's
	// add it on the top frame.
	// Why `visibilitychange` instead of `beforeunload`? See
	// https://developer.chrome.com/articles/page-lifecycle-api/#the-beforeunload-event
	if (navigator.userAgent.includes("Electron")) {
		const undo2 = onVisibilitychangeHidden(
			// Currently equivalent to `window.parent` in most cases.
			getTopmostUnisolatedWindow(),

			callback,
		);
		onUndo.push(undo2);
	}

	return () => { onUndo.forEach(cb => cb()); }
}

/**
 * @param {Window} win
 */
function onVisibilitychangeHidden(win, callback) {
	const listener = () => {
		if (win.document.visibilityState === "hidden") {
			callback();
		}
	};
	win.addEventListener("visibilitychange", listener);
	return () => win.removeEventListener("visibilitychange", listener);
}

function getTopmostUnisolatedWindow() {
	let lastWorking = window;
	while (true) {
		const newCandidate = lastWorking.parent;
		if (newCandidate === lastWorking) {
			// It's the topmost window
			return lastWorking;
		}

		/** @type {boolean} */
		let canAccessCandidate;
		try {
			canAccessCandidate = Boolean(lastWorking.addEventListener);
		} catch (e) {
			canAccessCandidate = false;
		}

		if (!canAccessCandidate) {
			return lastWorking;
		}

		lastWorking = newCandidate;
	}
}
